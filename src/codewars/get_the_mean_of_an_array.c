// Get the mean of an array
// https://www.codewars.com/kata/563e320cee5dddcf77000158

#include <stddef.h>

int get_average(const int *marks, size_t count) {
    size_t idx;
    double total = 0;

    for (idx = 0; idx < count; idx++) {
        total += marks[idx];
    }

    return (int) (total / count);
}
