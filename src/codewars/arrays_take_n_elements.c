// Arrays, take n elements
// https://www.codewars.com/kata/594fcafc0462da3aab0000a1

#include <stddef.h> /* NULL */
#include <stdlib.h>

int *ary_take(const int *ary, size_t ary_size, size_t n, size_t *res_size) {
    int* copy;
    size_t i;

    if (n == 0) {
        *res_size = 0;
        return NULL;
    }

    if (n < ary_size) {
        *res_size = n;
    }
    else {
        *res_size = ary_size;
    }

    copy = calloc(*res_size, sizeof(int));
    for (i = 0; i < *res_size; i++) {
        copy[i] = ary[i];
    }

    return copy;
}
