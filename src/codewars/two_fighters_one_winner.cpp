// Two Fighters, One Winner
// https://www.codewars.com/kata/577bd8d4ae2807c64b00045b

#include <string>
#include <iostream>

class Fighter
{
private:
    std::string name;
    
    int health;
    
    int damagePerAttack;

public:
    Fighter(std::string name, int health, int damagePerAttack)
    {
        this->name = name;
        this->health = health;
        this->damagePerAttack = damagePerAttack;
    }
    
    ~Fighter() { };
    
    std::string getName()
    {
        return name;
    }
    
    int getHealth()
    {
        return health;
    }
    
    int getDamagePerAttack()
    {
        return damagePerAttack;
    }
    
    void setHealth(int value)
    {
        health = value;
    }
};

std::string declareWinner(Fighter* fighter1, Fighter* fighter2, std::string firstAttacker)
{
    Fighter* nextFighter = firstAttacker == fighter1->getName() ? fighter1 : fighter2;
    while (fighter1->getHealth() > 0 && fighter2->getHealth() > 0) {
        if (nextFighter == fighter1) {
            fighter2->setHealth(fighter2->getHealth() - fighter1->getDamagePerAttack());
            nextFighter = fighter2;
        }
        else {
            fighter1->setHealth(fighter1->getHealth() - fighter2->getDamagePerAttack());
            nextFighter = fighter1;
        }
    }
    return fighter1->getHealth() > 0 ? fighter1->getName() : fighter2->getName();
}

int main() {
    Fighter fighter1("Lew", 10, 2);
    Fighter fighter2("Harry", 5, 4);

    std::cout << declareWinner(&fighter1, &fighter2, "Lew") << std::endl;

    return 0;
}
