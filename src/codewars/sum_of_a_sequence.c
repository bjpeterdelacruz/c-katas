// Sum of a sequence
// https://www.codewars.com/kata/586f6741c66d18c22800010a

unsigned sequence_sum(unsigned start, unsigned end, unsigned step) {
    unsigned sum = 0;
    unsigned idx = start;

    if (start > end) {
        return 0;
    }

    while (idx <= end) {
        sum += idx;
        idx += step;
    }
    return sum;
}
