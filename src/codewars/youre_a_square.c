// You're a square!
// https://www.codewars.com/kata/54c27a33fb7da0db0100040e

#include <stdbool.h>
#include <math.h>

bool is_square(int n) {
    int value = (int) pow(n, 0.5);
    return value * value == n;
}
