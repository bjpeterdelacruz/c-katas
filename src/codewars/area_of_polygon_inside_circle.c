// Calculate the area of a regular n sides polygon inside a circle of radius r
// https://www.codewars.com/kata/5a58ca28e626c55ae000018a

#include <math.h>

double PI = 3.141592653589793; // Use this value as pi in your calculations if necessary
double area_of_polygon_inside_circle(double circle_radius, int number_sides) {
    double result = 0.5 * number_sides * circle_radius * circle_radius * sin(2 * PI / number_sides);
    return round(result * 1000) / 1000;
}
