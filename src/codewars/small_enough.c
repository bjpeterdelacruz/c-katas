// Small enough? - Beginner
// https://www.codewars.com/kata/57cc981a58da9e302a000214

#include <stdbool.h>
#include <stddef.h>

bool small_enough(int *arr, size_t length, int limit) {
    size_t idx;

    for (idx = 0; idx < length; idx++) {
        if (arr[idx] > limit) {
            return false;
        }
    }

    return true;
}
