// 101 Dalmations - Squash the Bugs, Not the Dogs!
// https://www.codewars.com/kata/56f6919a6b88de18ff000b36

#include <vector>
#include <string>
#include <iostream>

std::string howManyDalmatians(int number) {
  std::vector<std::string> dogs;
  dogs.push_back("Hardly any");
  dogs.push_back("More than a handful!");
  dogs.push_back("Woah that's a lot of dogs!");
  dogs.push_back("101 DALMATIONS!!!");

  if (number <= 10) {
      return dogs[0];
  }
  if (number <= 50) {
      return dogs[1];
  }
  return number == 101 ? dogs[3] : dogs[2];
}

int main() {
    std::cout << howManyDalmatians(10) << std::endl;
    std::cout << howManyDalmatians(45) << std::endl;
    std::cout << howManyDalmatians(90) << std::endl;
    std::cout << howManyDalmatians(101) << std::endl;

    return 0;
}
