#define CATCH_CONFIG_MAIN

#include <stdlib.h>
#include <string.h>

char* toCamelCase(char* string) {
    int stringIdx = 0;
    int newStringIdx = 0;
    char* newString = (char*) calloc(strlen(string) + 1, sizeof(char));
    if (newString == NULL) {
        exit(1);
    }

    // Strings may start with more than one underscore.
    while (string[stringIdx++] == '_');
    if (string[--stringIdx] == '\0') {
        return newString;
    }
    newString[newStringIdx++] = string[stringIdx];

    while (string[stringIdx++]) {
        // There may be more than one underscore between words.
        while (string[stringIdx] == '_') {
            // Strings may end with an underscore.
            if (string[stringIdx + 1] == '\0') {
                return newString;
            }
            if (string[stringIdx + 1] != '_') {
                break;
            }
            stringIdx++;
        }
        newString[newStringIdx++] = string[stringIdx] == '_' ? string[++stringIdx] - 32 : string[stringIdx];
    }
    return newString;
}
